<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'film-wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'tmahmud');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(TWyMol@KN7p#HkW$}C]>Rr9*]NcS4Ck=iKG0&+h~t g.Y5P;yGX$sg=IQFV&-t?');
define('SECURE_AUTH_KEY',  '~W?0m,y/qrJ8k_t:zhk@+YFdx6ObeA)gMb67M:a(I>jYYjiK1BQCR?6$#gj24t[O');
define('LOGGED_IN_KEY',    'Z7~M%QMARv1aU7EAY#+9f{2DV$z_unU.*JxrW+0)`sSeUZ@#e}Xz=hCAR*kpO#mR');
define('NONCE_KEY',        '7`a<mU^g4&yz&8:qQ,*#;kb4q2pl Z7g~n~p7ZPHQnA&m`fn_$6]NYH*C7ZQ/`>Q');
define('AUTH_SALT',        '=iUMira{5;qPWLZL.*Ul@xN0jNu=KJXPt^A~bOJ5yZ*EG-nDSRk]:zqUa_U!AF.-');
define('SECURE_AUTH_SALT', 'J?HQP-fKv288Bc9&F%lPSJbE+sl>_ebFsp@YYU|QY}|OX~{9Aa$9F]XYfx5S7_<&');
define('LOGGED_IN_SALT',   'cQ4ND&MG3D|-.! (CWwRfNM Fia[4Hu_[6mS{DpVK:LDlfz5YjvO|VM8~ scT/6>');
define('NONCE_SALT',       '!T0k)ajF&zTO%p2!ZTE#N|:*&RZ+A.*E=YW1pU($[m<7wPE)ekSoi*U=7opSn=47');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
