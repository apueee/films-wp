<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 23/6/18
 * Time: 9:53 PM
 */

/**
 * Template Name: Films
 **/


get_header(); ?>

<div id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
    <main id="main" class="site-main" role="main">
        <?php

            $query = new WP_Query( array('post_type' => 'films', 'posts_per_page' => 5 ) );
            while ( $query->have_posts() ) : $query->the_post();
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail();
                }

                get_template_part( 'content', 'page-films' );

            ?>


            <?php wp_reset_postdata(); ?>
        <?php endwhile; ?>

<!--        --><?php //while ( have_posts() ) : the_post(); ?>
<!---->
<!--            --><?php //get_template_part( 'content', 'page' ); ?>
<!---->
<!--            --><?php
//            // If comments are open or we have at least one comment, load up the comment template
//            if ( comments_open() || '0' != get_comments_number() ) :
//                comments_template();
//            endif;
//            ?>
<!---->
<!--        --><?php //endwhile; // end of the loop. ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>

