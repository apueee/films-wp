<?php 
	 add_action( 'wp_enqueue_scripts', 'unite_child_enqueue_styles' );
	 add_action('init', 'films_init');
     add_action( 'init', 'create_films_taxonomies', 0 );
     add_action( 'films_meta_display', 'create_films_meta_display', 0 );

	 function unite_child_enqueue_styles() {
	     wp_enqueue_style( 'unite-style', get_template_directory_uri() . '/style.css' );
	     wp_enqueue_style( 'child-style',
             get_stylesheet_directory_uri() . '/style.css',
             array( 'unite-style' ),
             wp_get_theme()->get('Version')
        );
	 }

	 //Create Films Custom Post
     function films_init()
     {
         $args = [
             'label' => 'Films',
             'public' => true,
             'show_ui' => true,
             'capability_type' => 'post',
             'hierarchical' => false,
             'rewrite' => array('slug' => 'films'),
             'query_var' => true,
             'menu_icon' => 'dashicons-video-alt',
             'supports' => array(
                 'title',
                 'editor',
                 'excerpt',
                 'trackbacks',
                 'custom-fields',
                 'comments',
                 'revisions',
                 'thumbnail',
                 'author',
                 'page-attributes',)

         ];

         register_post_type('films', $args);
     }

    // create taxonomies Genre, Country, Year and Actorsfor the post type "films"
    function create_films_taxonomies() {
        register_taxonomy('films_genre', 'films',
            array(
                'labels' => array(
                    'name' => 'Movie Genre',
                    'add_new_item' => 'Add New Movie Genre',
                    'new_item_name' => "New Movie Type Genre"
                ),
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true
            )
        );

        register_taxonomy('films_country', 'films',
            array(
                'labels' => array(
                    'name' => 'Movie Country',
                    'add_new_item' => 'Add New Movie Country',
                    'new_item_name' => "New Movie Type Country"
                ),
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true
            )
        );

        register_taxonomy('films_actor', 'films',
            array(
                'labels' => array(
                    'name' => 'Movie Actor',
                    'add_new_item' => 'Add New Movie Actor',
                    'new_item_name' => "New Movie Type Actor"
                ),
                'show_ui' => true,
                'show_tagcloud' => false,
                'hierarchical' => true
            )
        );

        register_taxonomy('films_year', 'films',
            array(
                'hierarchical' => true,

                'labels' => array(
                    'name' => _x( 'Year', 'taxonomy general name' ),
                    'singular_name' => _x( 'Year', 'taxonomy singular name' ),
                    'search_items' =>  __( 'Search Years' ),
                    'all_items' => __( 'All Years' ),
                    'parent_item' => __( 'Parent Year' ),
                    'parent_item_colon' => __( 'Parent Year:' ),
                    'edit_item' => __( 'Edit Year' ),
                    'update_item' => __( 'Update Year' ),
                    'add_new_item' => __( 'Add New Year' ),
                    'new_item_name' => __( 'New Year Name' ),
                    'menu_name' => __( 'Years' ),
                ),
                'rewrite' => array(
                    'slug' => 'Years',
                    'with_front' => false,
                    'hierarchical' => true
                ),
        ));
    }

    function create_films_meta_display()
    {
        global $post;

        ?>
        <p>Ticket Price: $<?php echo get_post_meta($post->ID, 'ticket_price', true); ?>, Release Date:<?php echo get_post_meta($post->ID, 'release_date', true); ?></p>
            <p>
                <?php the_terms( $post->ID, 'films_year', 'Year: ', ' / ' );?>&nbsp;
                <?php the_terms( $post->ID, 'films_genre', 'Genres: ', ' / ' );?>&nbsp;
                <?php the_terms( $post->ID, 'films_actor', 'Actors: ', ' / ' );?>
            </p>
    <?php
    }

    function create_latest_films_shortcode($atts) {

        extract(shortcode_atts(array(
            "total" => '5'
        ), $atts));

        global $post;

        $latestFilms = get_posts('numberposts='.$total.'&order=DESC&orderby=post_date&post_type=films');

        $output = ['<ul>'];

        foreach($latestFilms as $post) :
            setup_postdata($post);
            $output[]='<li><a href="' . get_permalink() . '">' . the_title("","",false) . '</a></li>';
        endforeach;

        $output[]='</ul> ';

        return implode('', $output);
    }

    add_shortcode("latest-films", "create_latest_films_shortcode");
 ?>