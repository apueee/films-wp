<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package unite
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header page-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>


	</header><!-- .entry-header -->

	<div class="entry-content">
<!--        --><?php //if ( has_post_thumbnail() ) {
//                    the_post_thumbnail();
//                }?>
<!--        --><?php //if ( has_post_thumbnail()) the_post_thumbnail('excerpt-thumb'); ?>

		<?php the_excerpt(); ?>
        <?php do_action('films_meta_display'); ?>
        <br />
        <br />
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'unite' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'unite' ), '<footer class="entry-meta"><i class="fa fa-pencil-square-o"></i><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
